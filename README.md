# A.Team -- Job Scheduler

This is a possible solution for the Job Scheduler challenge.

## App architecture

The project is split up in two different parts: The API and the Job runner (Read on "Notes" more about this). The API exposes methods to interact with the jobs, while the job runner picks a job out of the database and runs it according to a set of rules.

The order on which the Job runner picks the tasks is based on its `priority`, and it's `nextExecutionAt` date (which is indirectly linked to the job's recurrence). Then, we use the `mongodb` database as a priority queue to fetch the corresponding jobs.

## Running the project

To develop, you can spin up a mongo instance using `docker-compose up db`. By default, it will use the environmental variables defined in the `.env` file. To run the server locally you'll have to install the dependencies (`npm install`) and then run `npm run start`. But you can also run the server in production mode using docker, by running `npm run build` and then `docker-compose up web`.

To play around with the project, the easiest way is to run:

```sh
docker-compose up web
```

Then you can connect with the API using postman on the following URL.

```
http://localhost:8080/api/v1/...
```

## API

The API is quite simple and exposes the following methods (incl. their verbs). The "executions" endpoint allows to list the historical execution events for a given job, with their statuses and outputs.

```
GET /api/v1/jobs
POST /api/v1/jobs/
GET /api/v1/jobs/:id
PATCH /api/v1/jobs/:id
GET /api/v1/jobs/:id/executions
```

The production URL is `https://a-team-scheduler.fly.dev`, and the local URL (with the default configuration) is http://localhost:8080.

## Testing

To test the API manually you can try creating a new job:

```sh
curl --location --request POST 'http://localhost:8080/api/v1/jobs' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "job",
    "script": "Math.random()",
    "recurrence": "immediate"
}'
```

You can also pass these fields:

```
"priority": Task priority, can be one of High, Mid, and Low.
"recurrence": When to run the task. Can be one of "daily", "weekly", "immediate", "date".
"nextExecutionAt": When to run the task if "recurrence" is set to "date"
```

You should be able to see logs similar to these in the server console:

```
[2022-05-18T23:17:52.335Z] START job 62857ea01693711b432c385c with execution 62857ea01693711b432c3860
[2022-05-18T23:18:22.342Z] SUCCEEDED 62857ea01693711b432c385c on execution 62857ea01693711b432c3860
```

Then you can try listing the executions to check the output of the execution

```sh
curl --location --request GET 'http://localhost:8080/api/v1/jobs/62857ea01693711b432c385c/executions'
```

The output should look similar to this:

```json
[
  {
    "id": "6285741dd8835d02b28f5e98",
    "status": "succeeded",
    "createdAt": "2022-05-18T22:33:01.055Z",
    "jobId": "62857ea01693711b432c385c",
    "output": "0.3428053675546443",
    "finishedAt": "Thu May 19 2022 18:33:01 GMT+0200 (Central European Summer Time)"
  }
]
```

## Assumptions

- Deleted jobs can't be listed, but are still accessible. This helps with traceability, and eliminates the need of cascading deleted.
- A Job's script can't be updated. This also helps with traceability. All historic executions point to the script it run, without having to duplicate data.

## Notes

In an ideal scenario, the job runner and the API would be in two different docker images altogether, so we can provide better security through isolation, so they can scale independently of each other, and so they don't compete for resources. Especially since the job runner is allowed to run (almost) arbitrary code. I did provide some isolation to the run the jobs through `vm2`, but that's not enough.

The `mongodb` database is exposed to the internet, although behind secure credentials. This is done for simplicity sake and is by no means a good practice to follow in real-life projects. I have not implemented any kind of auth processes in the application (server nor client side), as most of that code is pretty boilerplate-y.

`vm2` supports making external requests, but at the cost of losing automatic timeouts. I went with sync-only code to avoid collapsed servers, but could be easily reversed if we were to implement the timeout automatically.

The job runner is very simple, and as such doesn't have many checks that a real task runner would have. However, it still is performant enough that allows to run several async jobs at a time. The bottleneck is in synchronic loop-blocking code. Nevertheless, the runner is coded to be able to scale horizontally.

Test coverage is definitely not great, but I added some integration test cases. I spin up an in-memory mongodb to test the Job model.

Code quality is not great either lol, but I tried to prioritize the app architecture as I'd do in a customer-facing project.
