import * as express from "express";
import jobs from '~/endpoints/jobs';

const router = express.Router();

router.get("/", (req, res) => {
  res.status(200).send("A.Team Scheduler API v1");
});

router.use('/jobs', jobs);

export default router;