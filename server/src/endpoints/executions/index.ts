import Execution, { SerializedExecution } from "~/models/Execution";

// dependencies
import * as express from "express";

const router = express.Router();

router.get('/', async (req: express.Request<unknown, SerializedExecution[], unknown, { page: number }>, res: express.Response) => {
  const { page = 1 } = req.query;
  const { job } = req;

  if (!job) {
    res.status(404).send({});
    return;
  }

  const executions = await Execution.list(job.id, page);
  const serializedExecutions = executions.map((execution) => execution.serialized);

  res.status(200).send(serializedExecutions);
});

export default router;