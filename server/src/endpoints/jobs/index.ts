import Job, { InputJob, SerializedJob, UpdateJob } from "~/models/Job";

import { createObjectId } from "~/values/ObjectId";
import { createUpdateFromSerialized, createInputFromSerializable } from '~/parsers/Job'
import executions from '~/endpoints/executions';

// errors
import InconsistencyError from "~/errors/InconsistencyError";
import ValidationError from "~/errors/ValidationError";

// dependencies
import * as express from "express";

const router = express.Router();

router.get('/', async (req: express.Request<unknown, SerializedJob[], unknown, { page: number }>, res: express.Response) => {
  const { page = 1 } = req.query;

  const jobs = await Job.list(page);
  const serializedJobs = jobs.map((job) => job.serialized);

  res.status(200).send(serializedJobs);
});

router.post('/', async (req: express.Request<unknown, SerializedJob, unknown, unknown>, res: express.Response) => {
  if (typeof req.body !== 'object' || req.body === null) {
    res.status(400).send('Invalid body');
    return;
  }

  try {
    const input: InputJob = createInputFromSerializable(req.body as Record<string, unknown>);
    const job = await Job.create(input);
    res.status(200).send(job.serialized);
  } catch (e) {
    if (e instanceof ValidationError) {
      res.status(400).send(e.message);
      return;
    } else if (e instanceof InconsistencyError) {
      res.status(400).send(e.message);
      return;
    }

    throw e;
  }
});

router.get('/search', async (req: express.Request<unknown, SerializedJob[], unknown, { page: number, token: string }>, res: express.Response) => {
  const { page = 1, token } = req.query;

  const jobs = await Job.list(page, token);
  const serializedJobs = jobs.map((job) => job.serialized);

  res.status(200).send(serializedJobs);
});

router.get('/:jobId', async (req: express.Request<{ jobId: string }, SerializedJob, unknown, unknown>, res: express.Response) => {
  const jobId = createObjectId(req.params.jobId);
  if (!jobId) {
    res.status(400).send('Invalid job id');
    return;
  }

  const job = await Job.fetch(jobId);
  if (!job) {
    res.status(404).send({});
    return;
  }

  res.status(200).send(job.serialized);
});

router.patch('/:jobId', async (req: express.Request<{ jobId: string }, SerializedJob, unknown, unknown>, res: express.Response) => {
  const jobId = createObjectId(req.params.jobId);
  if (!jobId) {
    res.status(400).send('Invalid job id');
    return;
  } else if (typeof req.body !== 'object' || req.body === null) {
    res.status(400).send('Invalid body');
    return;
  }

  const job = await Job.fetch(jobId);
  if (!job) {
    res.status(404).send({});
    return;
  }

  try {
    const updates: UpdateJob = createUpdateFromSerialized(req.body as Record<string, unknown>);
    const updatedJob = await job.update(updates);
    res.status(200).send(updatedJob.serialized);
    return;
  } catch (e) {
    if (e instanceof ValidationError) {
      res.status(400).send(e.message);
      return;
    } else if (e instanceof InconsistencyError) {
      res.status(404).send(e.message);
      return;
    }

    throw e;
  }
});

router.delete('/:jobId', async (req: express.Request<{ jobId: string }, unknown, unknown, unknown>, res: express.Response) => {
  const jobId = createObjectId(req.params.jobId);
  if (!jobId) {
    res.status(400).send('Invalid job id');
    return;
  }

  const job = await Job.fetch(jobId);
  if (!job) {
    res.status(404).send({});
    return;
  }

  await job.delete();

  res.status(200).send({});
});

router.use('/:jobId/executions', async (req, res, next) => {
  const jobId = createObjectId(req.params.jobId);
  if (!jobId) {
    res.status(400).send('Invalid job id');
    return;
  }

  const job = await Job.fetch(jobId);
  if (!job) {
    res.status(404).send({});
    return;
  }

  // preserve job through routers
  req.job = job;

  executions(req, res, next);
});

export default router;