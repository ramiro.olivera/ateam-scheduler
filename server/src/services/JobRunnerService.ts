// models
import Job from '~/models/Job';
import Execution from '~/models/Execution';

// dependencies
import { VM } from 'vm2';

const COOLDOWN_TIME = 100;

const sleep = (ms: number) => new Promise((res) => setTimeout(res, ms));

const run = (script: string) => {
  // create sandbox
  const vm = new VM({
    timeout: Number(process.env.JOB_TIMEOUT),
    allowAsync: false,
    sandbox: {}
  });

  // run the script in the sandbox
  return vm.run(script)
}

export const start = async (): Promise<never> => {
  // eslint-disable-next-line
  while (true) {
    // fetch a job
    const job = await Job.pop();

    // if there is no job to be processed
    // retry fetching after waiting some time to avoid collapsing the connection
    if (!job) {
      await sleep(COOLDOWN_TIME);
      continue;
    }

    // run the script
    const execution = await Execution.createFromJob(job);
    try {
      console.log(`[${new Date().toISOString()}] START job ${job.id} with execution ${execution.id}`);
      const output = run(job.script);
      await execution.succeed(output);
      console.log(`[${new Date().toISOString()}] SUCCEEDED ${job.id} on execution ${execution.id}`);
    } catch (e) {
      console.log(`[${new Date().toISOString()}] FAILED ${job.id} on execution ${execution.id}`);
      await execution.fail(e instanceof Error ? e.message : 'Unexpected error');
    }
  }
}