export default class InconsistencyError extends Error {
  constructor(m: string) {
    super(m);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, InconsistencyError.prototype);
  }
}