declare class MongoError extends Error {
  public code: number;
}

export const MongoErrorTypes = {
  DuplicatedKey: 11000,
}

export const isMongoError = (e: unknown): e is MongoError => {
  if (typeof e === 'object' && e && 'code' in e) {
    return true;
  }

  return false;
}