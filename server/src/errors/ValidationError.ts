export default class ValidationError extends Error {
  readonly fields: Record<string, unknown> = {};

  constructor(m: string, fields: Record<string, unknown>) {
    super(m);

    this.fields = Object.freeze({ ...fields });

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ValidationError.prototype);
  }
}