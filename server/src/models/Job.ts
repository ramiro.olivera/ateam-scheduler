// dependencies
import { Schema, Types, model } from 'mongoose';
import { addDays, isToday, isThisWeek } from 'date-fns'

// errors
import ValidationError from '~/errors/ValidationError';
import InconsistencyError from '~/errors/InconsistencyError';
import { MongoErrorTypes } from '~/errors/MongoError';

// value objects
import { Recurrence } from '~/values/Recurrence';
import { Priority, serializePriority } from '~/values/Priority';
import { Status } from '~/values/Status';
import { isMongoError } from '~/errors/MongoError';

const PageLimit = 10

interface ValidJob {
  _id: Types.ObjectId
  name: string,
  script: string,
  recurrence: Recurrence,
  nextExecutionAt: Date,
  status: Status,
  priority: Priority,
  createdAt: Date,
}

export interface InputJob {
  name: ValidJob['name'],
  script: ValidJob['script'],
  recurrence: ValidJob['recurrence'],
  status?: ValidJob['status'],
  nextExecutionAt?: ValidJob['nextExecutionAt']
  priority?: ValidJob['priority'],
  createdAt?: ValidJob['createdAt'],
}

export interface UpdateJob {
  name?: ValidJob['name'],
  recurrence?: ValidJob['recurrence'],
  nextExecutionAt?: ValidJob['nextExecutionAt']
  priority?: ValidJob['priority'],
}

export interface SerializedJob {
  id: string,
  name: ValidJob['name'],
  script: ValidJob['script'],
  recurrence: ValidJob['recurrence'],
  status: ValidJob['status'],
  priority: string,
  nextExecutionAt: string,
  createdAt: string,
}

const schema = new Schema<ValidJob>({
  name: { type: String, required: true, unique: true },
  script: { type: String, required: true },
  status: { type: String, default: 'idle' },
  recurrence: { type: String, required: true },
  priority: { type: Number, default: Priority.Low },
  nextExecutionAt: { type: Date, default: Date.now },
  createdAt: { type: Date, default: Date.now },
});

schema.index({ status: 1, name: 1 });
schema.index({ status: 1, nextExecutionAt: 1, priority: -1 });

export class Job {
  private static model = model<ValidJob>('Job', schema);
  private readonly properties: ValidJob;

  private constructor(properties: ValidJob) {
    this.properties = properties;
  }

  static create = async (job: InputJob): Promise<Job> => {
    if (job.recurrence === 'date' && !job.nextExecutionAt) {
      throw new ValidationError('Execution date is missing', {
        recurrence: job.recurrence,
        nextExecutionAt: job.nextExecutionAt,
      })
    }

    try {
      const persistedJob = await Job.model.create({
        name: job.name,
        script: job.script,
        recurrence: job.recurrence,
        ...job.status && { status: job.status },
        ...job.priority && { priority: job.priority },
        ...job.createdAt && { createdAt: job.createdAt },
        ...job.recurrence === 'date' && { nextExecutionAt: job.nextExecutionAt },
      })

      return new Job(persistedJob);
    } catch (e) {
      if (isMongoError(e) && e.code === MongoErrorTypes.DuplicatedKey) {
        throw new InconsistencyError('Job already exists');
      }

      throw e;
    }
  }

  static pop = async (): Promise<Job | null> => {
    // fetch the next job to process according to date and priority
    const dbJob = await Job.model.findOneAndUpdate({
      status: 'idle',
      nextExecutionAt: { $lte: new Date() },
    }, {
      $set: { status: 'processing' }, // we need this state to avoid race-conditions
    }, {
      new: true,
      sort: { priority: -1, nextExecutionAt: 1 },
    });

    if (!dbJob) {
      return null;
    }

    // reschedule job
    const job = new Job(dbJob);
    const dbRescheduledJob = await job.reschedule();
    if (!dbRescheduledJob) {
      throw new InconsistencyError(`The job ${job.properties._id} does not exist in the database`);
    }

    return new Job(dbRescheduledJob);
  }

  static list = async (page = 1, name?: string) => {
    const dbJobs = await Job.model.find({
      status: { $ne: 'deleted' },
      ...name && { name: new RegExp(name, 'i') },
    }).sort({ priority: -1, nextExecutionAt: 1 })
      .skip((page - 1) * PageLimit)
      .limit(PageLimit);

    return dbJobs.map((dbJob) => new Job(dbJob));
  }

  static fetch = async (jobId: Types.ObjectId) => {
    const dbJob = await Job.model.findOne({ _id: jobId });
    if (!dbJob) {
      return null;
    }

    const job = new Job(dbJob);
    return job;
  }

  private reschedule = async () => {
    if (this.properties.recurrence === 'immediate') {
      return Job.model.findOneAndUpdate({ _id: this.properties._id }, { $set: { status: 'done' } }, { new: true });
    } else if (this.properties.recurrence === 'date') {
      return Job.model.findOneAndUpdate({ _id: this.properties._id }, { $set: { status: 'done' } }, { new: true });
    } else if (this.properties.recurrence === 'daily') {
      return Job.model.findOneAndUpdate({ _id: this.properties._id }, {
        $set: {
          status: 'idle',
          nextExecutionAt: addDays(new Date(), 1),
        }
      }, { new: true });
    } else if (this.properties.recurrence === 'weekly') {
      return Job.model.findOneAndUpdate({ _id: this.properties._id }, {
        $set: {
          status: 'idle',
          nextExecutionAt: addDays(new Date(), 7),
        }
      }, { new: true });
    }
  }

  delete = async () => {
    await Job.model.findOneAndUpdate({ _id: this.properties._id }, {
      $set: { status: 'deleted' }
    })
  }

  update = async (updates: UpdateJob) => {
    // if changing the execution to 
    if (updates.recurrence === 'date' && !updates.nextExecutionAt) {
      throw new ValidationError('Execution date is missing', {
        recurrence: updates.recurrence,
        nextExecutionAt: updates.nextExecutionAt,
      })
    }

    const updatesToApply: UpdateJob = {
      ...updates.name && { name: updates.name },
      ...updates.priority && { priority: updates.priority },
      ...updates.recurrence && { recurrence: updates.recurrence },
    }

    const dbOutdatedJob = await Job.model.findOne({
      _id: this.properties._id,
      status: { $nin: ['done', 'deleted'] }
    });
    if (!dbOutdatedJob) {
      throw new InconsistencyError(`The job ${this.properties._id} can't be updated`);
    }
    const outdatedJob = new Job(dbOutdatedJob);

    // if there are changes to the recurrence of the job, we have to calculate the next execution date too
    if (updates.recurrence === 'immediate') {
      updatesToApply.nextExecutionAt = new Date();
    } else if (updates.recurrence === 'date') {
      updatesToApply.nextExecutionAt = updates.nextExecutionAt;
    } else if (updates.recurrence === 'daily' && !isToday(outdatedJob.properties.nextExecutionAt)) {
      // if it is going to be executed today, we can leave the next execution day as-is. but, if
      // the execution date is later than that, we have to run it today.
      updatesToApply.nextExecutionAt = new Date();
    } else if (updates.recurrence === 'weekly' && !isThisWeek(outdatedJob.properties.nextExecutionAt)) {
      // if it is going to be executed this week, we can leave the next execution day as-is. but, if
      // the execution date is later than that, we have to run it today.
      updatesToApply.nextExecutionAt = new Date();
    }

    try {
      const dbJob = await Job.model.findOneAndUpdate({ _id: this.properties._id }, { $set: updatesToApply }, { new: true });
      if (!dbJob) {
        throw new InconsistencyError(`The job ${this.properties._id} does not exist in the database`);
      }

      return new Job(dbJob);
    } catch (e) {
      if (isMongoError(e) && e.code === MongoErrorTypes.DuplicatedKey) {
        throw new InconsistencyError('A job with this name already exists');
      }

      throw e;
    }
  }

  stop = async () => {
    await Job.model.findOneAndUpdate({ _id: this.properties._id }, {
      $set: { status: 'stopped' }
    })
  }

  get serialized(): SerializedJob {
    return {
      id: this.properties._id.toString(),
      name: this.properties.name,
      script: this.properties.script,
      status: this.properties.status,
      recurrence: this.properties.recurrence,
      priority: serializePriority(this.properties.priority),
      nextExecutionAt: this.properties.nextExecutionAt.toISOString(),
      createdAt: this.properties.nextExecutionAt.toISOString(),
    }
  }

  get id() {
    return this.properties._id;
  }

  get script() {
    return this.properties.script
  }
}

export default Job;