// models
import Job from '~/models/Job';

// dependencies
import { Schema, Types, model } from 'mongoose';

const PageLimit = 20;

export interface SerializedExecution {
  id: string,
  status: 'succeeded' | 'failed' | 'running',
  createdAt: string,
  jobId: string,
  output?: string,
  finishedAt?: string,
}

interface ValidExecution {
  _id: Types.ObjectId
  status: 'succeeded' | 'failed' | 'running',
  createdAt: Date,
  jobId: Types.ObjectId,
  output?: string,
  finishedAt?: Date,
}

export interface InputExecution {
  jobId: Types.ObjectId,
}

const schema = new Schema<ValidExecution>({
  status: { type: String, default: 'running' },
  createdAt: { type: Date, default: Date.now },
  jobId: { type: Schema.Types.ObjectId, required: true },
  finishedAt: { type: Date },
  output: { type: String },
});

schema.index({ jobId: 1 });

export class Execution {
  private static model = model<ValidExecution>('Execution', schema);
  private readonly properties: ValidExecution;

  private constructor(properties: ValidExecution) {
    this.properties = properties;
  }

  static createFromJob = async (job: Job): Promise<Execution> => {
    const persistedExecution = await Execution.model.create({
      jobId: job.id,
    });

    return new Execution(persistedExecution);
  }

  static list = async (jobId: Types.ObjectId | string, page = 1) => {
    const dbExecutions = await Execution.model.find({ jobId })
      .sort({ createdAt: -1 })
      .skip((page - 1) * PageLimit)
      .limit(PageLimit);

    return dbExecutions.map((dbExecution) => new Execution(dbExecution));
  }

  fail = async (reason: string) => {
    await Execution.model.updateOne({ _id: this.properties._id }, {
      $set: { finishedAt: new Date(), status: 'failed', output: reason },
    });
  }

  succeed = async (output: string) => {
    await Execution.model.updateOne({ _id: this.properties._id }, {
      $set: { finishedAt: new Date(), status: 'succeeded', output },
    });
  }

  get serialized(): SerializedExecution {
    return {
      id: this.properties._id.toString(),
      status: this.properties.status,
      createdAt: this.properties.createdAt.toISOString(),
      jobId: this.properties.jobId.toJSON(),
      ...this.properties.output && { output: this.properties.output },
      ...this.properties.finishedAt && { finishedAt: this.properties.finishedAt.toString() },
    }
  }

  get id() {
    return this.properties._id;
  }
}

export default Execution;