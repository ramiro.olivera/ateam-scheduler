// models
import { InputJob, UpdateJob } from '~/models/Job';

// errors
import ValidationError from '~/errors/ValidationError';

// value objects
import { createStatus } from '~/values/Status';
import { createRecurrence } from '~/values/Recurrence';
import { createPriority } from '~/values/Priority';

// dependencies
import { isValid } from 'date-fns';

export const createInputFromSerializable = (input: Record<string, unknown>): InputJob => {
  const recurrence = typeof input.recurrence === 'string' ? createRecurrence(input.recurrence) : null;
  const priority = typeof input.priority === 'string' ? createPriority(input.priority) : null;
  const status = typeof input.status === 'string' ? createStatus(input.status) : null;
  const nextExecutionAt = typeof input.nextExecutionAt === 'string' ? new Date(input.nextExecutionAt) : null;
  const createdAt = typeof input.createdAt === 'string' ? new Date(input.createdAt) : null;

  if (typeof input.name !== 'string') {
    throw new ValidationError('invalid name field', { input });
  } else if (typeof input.script !== 'string') {
    throw new ValidationError('invalid script field', { input });
  } else if (recurrence === null) {
    throw new ValidationError('invalid recurrence field', { input });
  } else if ('nextExecutionAt' in input && nextExecutionAt === null) {
    throw new ValidationError('invalid nextExecutionAt field', { input });
  } else if ('createdAt' in input && createdAt === null) {
    throw new ValidationError('invalid createdAt field', { input });
  } else if ('priority' in input && priority === null) {
    throw new ValidationError('invalid priority field', { input });
  } else if ('status' in input && status === null) {
    throw new ValidationError('invalid status field', { input });
  }

  return {
    name: input.name,
    script: input.script,
    ...recurrence && { recurrence },
    ...priority && { priority },
    ...status && { status },
    ...nextExecutionAt && { nextExecutionAt },
    ...createdAt && { createdAt },
  };
}

export const createUpdateFromSerialized = (input: Record<string, unknown>): UpdateJob => {
  const recurrence = typeof input.recurrence === 'string' ? createRecurrence(input.recurrence) : null;
  const nextExecutionAt = typeof input.nextExecutionAt === 'string' ? new Date(input.nextExecutionAt) : null;
  const priority = typeof input.priority === 'string' ? createPriority(input.priority) : null;
  const name = typeof input.name === 'string' ? input.name : null;

  if ('name' in input && name === null) {
    throw new ValidationError('invalid name field', { input });
  } else if ('recurrence' in input && recurrence === null) {
    throw new ValidationError('invalid recurrence field', { input });
  } else if ('nextExecutionAt' in input && (nextExecutionAt === null || !isValid(nextExecutionAt))) {
    throw new ValidationError('invalid nextExecutionAt field', { input });
  } else if ('priority' in input && priority === null) {
    throw new ValidationError('invalid priority field', { input });
  } else if ('script' in input) {
    throw new ValidationError('cannot update script', { input });
  }

  return {
    ...name && { name },
    ...recurrence && { recurrence },
    ...priority && { priority },
    ...nextExecutionAt && { nextExecutionAt },
  };
}