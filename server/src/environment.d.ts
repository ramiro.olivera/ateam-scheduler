import Job from "./models/Job";

declare global {
  type Stringify<T> = {
    [property in keyof T]: string;
  };

  namespace NodeJS {
    interface ProcessEnv {
      DB_CONNECTION_STRING: string;
      NODE_ENV: 'development' | 'production';
      SERVER_PORT: string;
      JOB_TIMEOUT: number;
    }
  }

  namespace Express {
    export interface Request {
      job?: Job;
    }
  }
}
