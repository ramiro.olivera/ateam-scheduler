// dependencies
import dotenv from "dotenv";
import express from 'express';
import morgan from 'morgan';
import { connect } from 'mongoose';

// endpoints
import endpoints from '~/endpoints';

// services
import * as JobRunnerService from '~/services/JobRunnerService';

dotenv.config();

// middleware
const app = express();
app.use(morgan('combined'));
app.use(express.json());
app.use('/api/v1', endpoints);

// startup server
connect(process.env.DB_CONNECTION_STRING).then(() => {
  JobRunnerService.start().catch((e) => console.error(e));
  app.listen(process.env.SERVER_PORT, () => {
    console.log(`✅ Server started at http://localhost:${process.env.SERVER_PORT}`);
  })
}).catch((e) => console.error(e));