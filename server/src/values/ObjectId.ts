import { Types } from 'mongoose';

export const createObjectId = (id: string | Types.ObjectId): Types.ObjectId | null => {
  if (typeof id === 'object') {
    return id;
  } else if (!Types.ObjectId.isValid(id)) {
    return null;
  }

  return new Types.ObjectId(id);
}