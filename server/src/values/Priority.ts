export enum Priority {
  Low = 0,
  Mid,
  High,
}

export const createPriority = (priority: string): Priority | null => {
  const map: Record<string, Priority> = {
    Low: Priority.Low,
    Mid: Priority.Mid,
    High: Priority.High,
  };

  if (priority in map) {
    return map[priority];
  }

  return null
}

export const serializePriority = (priority: Priority): string => {
  const map: Record<Priority, string> = {
    [Priority.Low]: 'Low',
    [Priority.Mid]: 'Mid',
    [Priority.High]: 'High',
  };

  return map[priority];
}