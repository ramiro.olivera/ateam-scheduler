export type Recurrence = 'immediate' | 'weekly' | 'daily' | 'date';

export const createRecurrence = (recurrence: string): Recurrence | null => {
  if (['immediate', 'weekly', 'daily', 'date'].includes(recurrence)) {
    return recurrence as Recurrence;
  }

  return null
}