export type Status = 'deleted' | 'done' | 'stopped' | 'idle' | 'processing';

export const createStatus = (status: string): Status | null => {
  if (['deleted', 'done', 'stopped', 'idle', 'processing'].includes(status)) {
    return status as Status;
  }

  return null
}