import * as dbHandler from '../utils/db-handler';
import Job from '../../src/models/Job';
import mongoose from 'mongoose';
import { addDays, isAfter, isBefore } from 'date-fns';

const sleep = (ms: number) => new Promise((res) => setTimeout(res, ms));

beforeAll(async () => await dbHandler.connect());
afterEach(async () => await dbHandler.clearDatabase());
afterAll(async () => await dbHandler.closeDatabase());

describe('job ', () => {
  const model = mongoose.models.Job;

  it('creates a job with default parameters', async () => {
    const before = new Date();

    await sleep(20); // small timeout to avoid equal timestamps

    const job: Job = await Job.create({
      name: 'test',
      script: '2+2',
      recurrence: 'immediate'
    })
    const after = new Date();

    expect(job).toBeInstanceOf(Job);
    expect(mongoose.Types.ObjectId.isValid(job.id)).toBe(true);
    expect(job.script).toBe('2+2');

    const dbJob = await model.findById(job.id);
    expect(dbJob).toMatchObject({
      name: 'test',
      script: '2+2',
      status: 'idle',
      recurrence: 'immediate',
      priority: 0,
    });

    expect(isBefore(dbJob.nextExecutionAt, after)).toBe(true);
    expect(isAfter(dbJob.nextExecutionAt, before)).toBe(true);
  });

  it('creates a job with custom parameters', async () => {
    const before = new Date();

    await sleep(20); // small timeout to avoid equal timestamps

    const job: Job = await Job.create({
      name: 'test',
      script: '2+2',
      priority: 2,
      recurrence: 'weekly'
    })
    const after = new Date();

    expect(job).toBeInstanceOf(Job);
    expect(mongoose.Types.ObjectId.isValid(job.id)).toBe(true);
    expect(job.script).toBe('2+2');

    const dbJob = await model.findById(job.id);
    expect(dbJob).toMatchObject({
      name: 'test',
      script: '2+2',
      status: 'idle',
      recurrence: 'weekly',
      priority: 2,
    });

    expect(isBefore(dbJob.nextExecutionAt, after)).toBe(true);
    expect(isAfter(dbJob.nextExecutionAt, before)).toBe(true);
  });

  it('deletes a job correctly', async () => {
    const job: Job = await Job.create({
      name: 'test',
      script: '2+2',
      recurrence: 'weekly'
    })

    await job.delete();

    const dbJob = await model.findById(job.id);
    expect(dbJob).toMatchObject({
      name: 'test',
      status: 'deleted',
    });
  });

  it('fetches jobs in the correct order', async () => {
    const jobs: Job[] = await Promise.all([
      Job.create({
        name: 'a',
        script: '2+2',
        priority: 1,
        recurrence: 'immediate'
      }),
      Job.create({
        name: 'b',
        script: '2+2',
        priority: 0,
        recurrence: 'immediate'
      }),
      Job.create({
        name: 'c',
        script: '2+2',
        priority: 2,
        recurrence: 'immediate'
      }),
      Job.create({
        name: 'd',
        script: '2+2',
        priority: 2,
        recurrence: 'immediate'
      }),
    ]);

    // update the "nextExecutionAt" to generate conflicts
    const overlapTimestamp = new Date();
    await model.findByIdAndUpdate(jobs[0].id, {
      $set: { nextExecutionAt: overlapTimestamp }
    });
    await model.findByIdAndUpdate(jobs[1].id, {
      $set: { nextExecutionAt: overlapTimestamp }
    });
    await model.findByIdAndUpdate(jobs[2].id, {
      $set: { nextExecutionAt: overlapTimestamp }
    });
    await model.findByIdAndUpdate(jobs[3].id, {
      $set: { nextExecutionAt: addDays(overlapTimestamp, 1) }
    });

    const poppedJobs = [await Job.pop(), await Job.pop(), await Job.pop(), await Job.pop()];

    // the first three are in the order of priority since all their timestamps matches
    // the last one is null, because even though it's priority is high, it's not yet time
    // to process it
    expect(poppedJobs[0]?.id).toStrictEqual(jobs[2].id);
    expect(poppedJobs[1]?.id).toStrictEqual(jobs[0].id);
    expect(poppedJobs[2]?.id).toStrictEqual(jobs[1].id);
    expect(poppedJobs[3]).toStrictEqual(null);
  });

  it('updates an object recurrence to "date" correctly', async () => {
    const job = await Job.create({
      name: 'a',
      script: '2+2',
      priority: 1,
      recurrence: 'weekly',
      nextExecutionAt: new Date(),
    });

    const newDate = addDays(new Date(), 20);
    const updatedJob = await job.update({ recurrence: 'date', nextExecutionAt: newDate });

    const dbJob = await model.findById(updatedJob.id);
    expect(dbJob).toMatchObject({
      recurrence: 'date',
      nextExecutionAt: newDate,
    })
  });

  it('updates an object recurrence to "immediate" correctly', async () => {
    const job = await Job.create({
      name: 'a',
      script: '2+2',
      priority: 1,
      recurrence: 'date',
      nextExecutionAt: addDays(new Date(), 20),
    });

    const updatedJob = await job.update({ recurrence: 'immediate' });

    const dbJob = await model.findById(updatedJob.id);
    expect(dbJob.recurrence).toBe('immediate');
    expect(isBefore(dbJob.nextExecutionAt, new Date())).toBe(true);
  });
});