import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

const mongodPromise = MongoMemoryServer.create();

// connect to the db
export const connect = async () => {
  const mongod = await mongodPromise;
  const uri = await mongod.getUri();
  await mongoose.connect(uri);
}

// drop the db
export const closeDatabase = async () => {
  const mongod = await mongodPromise;
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongod.stop();
}

// clean the db
export const clearDatabase = async () => {
  const collections = mongoose.connection.collections;

  for (const key in collections) {
    const collection = collections[key];
    await collection.deleteMany({});
  }
}